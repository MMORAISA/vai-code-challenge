# Vai Code Challenge

Olá,

Desenvolvi esse teste me preocupando em demonstrar uma boa lógica com a autenticação e controles de estados no Redux. O fluxo desenvolvido permite que o usuário atualize a tela, ou até mesmo feche ela e volte depois através da revalidação do token armazenado no localStorage.

Não tive tempo o suficiente para terminar os pontos levantados principalmente por que essa semana foi bastante corrida, mas acredito que o que foi desenvolvido até então é o suficiente para a minha avaliação.

Não entendi a ideia de utilizar o randomuser para login, e para as operações do CRUD, por isso fixei o login com o usuário abaixo em um arquivo mockado. Gostaria de adicionar que todas as operações estão sendo tratadas pelo Redux, com o controle de side-effects do Redux Saga e fazem chamadas em classes de integração que podem se conectar facilmente à serviços REST. Para o desenvolvimento do teste, optei por utilizar a versão 16.0.7-alpha.2 do React, afim de demonstrar os meus conhecimentos com tecnologias de ponta, como o React Hooks, que será disponibilizado em breve numa versão estável da biblioteca.

Como o tempo ficou apertado, decidi implementar alguns testes que eu considero importantes por serem diretamente ligados ao estado da aplicação à fim de demonstração: os testes estão no arquivo ducks/Users.test.js

Usuário
login: admin
senha: password

Estou disponível para quaisquer dúvidas ou testes presenciais.
Obrigado :)