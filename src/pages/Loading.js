import React, { useEffect } from 'react';
import { Spin } from 'antd';
import './Loading.css';
import { connect } from 'react-redux';
import { revalidateToken } from '../ducks/Auth';
import { setAuthUser, getAuthUser } from '../utility/authState';

function Loading({ auth, callRevalidateToken, history }) {

  useEffect(() => {
    if (auth.user) {
      setAuthUser(auth.user);
      history.push(history.location.state.from);
    }
    else {
      const user = getAuthUser();
      if (user) {
        callRevalidateToken(user.token);
      } 
      else {
        history.push('/login');
      }
    }
  }, [auth.user]);

  return (
    <Spin className="loading" />
  )
}

export default connect(
  ({ auth }) => ({ auth }),
  dispatch => ({
    callRevalidateToken(token) {
      dispatch(revalidateToken(token));
    }
  })
)(Loading);