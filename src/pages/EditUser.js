import React, { Fragment, useEffect, useState } from 'react';
import { Button, Popconfirm } from 'antd';
import { connect } from 'react-redux';
import LogoutButton from '../components/LogoutButton';
import { fetchOne, deleteUser } from '../ducks/Users';
import './EditUser.css';

function EditUser ({ callDelete, callFetchOne, history, match, users }) {

    const { uuid } = match.params;

    const [editingUser,setEditingUser] = useState(null);

    useEffect(() => {
        const user = users.data.find(user => user.login.uuid === uuid);

        if(!user) {
            callFetchOne(uuid);
        }
        else {
            setEditingUser(user);
        }
    },[uuid,users.data]);

    const handleDelete = uuid => {
        history.push('/home');
        callDelete(uuid);
    }

    return (
        <Fragment>
            <div className="edit-user-header">
                <h1>Editar usuário</h1>
                <div>
                    <Button
                        type="primary"
                        onClick={() => history.push('/home')}>
                        Voltar p/ listagem
                    </Button>
                    <LogoutButton history={history} />
                </div>
            </div>
            <div className="user-data">
                <h2>Dados do usuário</h2>
                {JSON.stringify(editingUser)}
            </div>
            <Popconfirm
                placement="leftTop"
                title={"Você tem certeza que deseja excluir esse usuário?"}
                onConfirm={() => handleDelete(uuid)}
                okText="Sim"
                cancelText="Não">
                <Button icon="delete" type="dashed">Excluir</Button>
            </Popconfirm>
        </Fragment>
    )

}

export default connect(
    ({ users }) => ({ users }),
    dispatch => ({
        callDelete (uuid) {
            dispatch(deleteUser(uuid));
        },
        callFetchOne (uuid) {
            dispatch(fetchOne(uuid));
        }
    })
)(EditUser);