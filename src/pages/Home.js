import React, { Fragment } from 'react';
import UserList from '../components/UserList';
import LanguagePicker from '../components/LanguagePicker';
import { ENABLED_NATIONALITIES } from '../utility/constants';
import { connect } from 'react-redux';
import { languageChange } from '../ducks/Auth';
import './Home.css';
import LogoutButton from '../components/LogoutButton';

function Home({ auth, callLanguageChange, history, users }) {
  return (
    <Fragment>
      <div className="home-header">
        <h1>Página Inicial</h1>
        <div>
          <LanguagePicker
            enabledNationalities={ENABLED_NATIONALITIES}
            onChangeNationality={callLanguageChange} />
          <LogoutButton history={history} />
        </div>
      </div>
      <UserList
        onUserClick={user => history.push(`/edit/${user.login.uuid}`)}
        users={users.data}
        loading={users.loading.fetch}
        language={auth.user.language} />
    </Fragment>
  )
}

export default connect(
  ({ auth, users }) => ({ auth, users }),
  dispatch => ({
    callLanguageChange(nationality) {
      dispatch(languageChange(nationality))
    }
  })
)(Home);