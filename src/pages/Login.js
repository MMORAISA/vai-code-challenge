import React, { useEffect } from 'react';
import { Button, Col, Form, Input, Row } from 'antd';
import { connect } from 'react-redux';
import { login } from '../ducks/Auth';
import { setAuthUser } from '../utility/authState';
import './Login.css';

const FormItem = Form.Item;

function Login({ auth, callLogin, form, history }) {

  const { getFieldDecorator } = form;

  const onLoginSubmit = (e) => {
    e.preventDefault();

    form.validateFields((err, values) => {
      if (!err) {
        const { username, password } = values;
        callLogin(username, password);
      }
    });
  }

  useEffect(() => {
    if(auth.user) {
      setAuthUser(auth.user);
      history.push('/home');
    }
  },[auth.user]);
  
  return (
    <Row className="row-form">
      <Col xs={{ span: 22, offset: 1 }} sm={{ span: 12, offset: 6 }} md={{ span: 8, offset: 8 }} lg={{ span: 6, offset: 9 }}>
        <h1>Acesso</h1>
        <Form className="form-login" onSubmit={(e) => onLoginSubmit(e, form)}>
          <FormItem>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Não se esqueça de preencher o usuário!' }]
            })(
              <Input disabled={auth.loading.login} type="text" placeholder="Usuário" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Não se esqueça de preencher a senha!' }]
            })(
              <Input disabled={auth.loading.login} type="password" placeholder="Senha" />
            )}
          </FormItem>
          <FormItem>
            <Button
              htmlType="submit"
              icon="login"
              loading={auth.loading.login}
              type="primary">
              Entrar
            </Button>
            { auth.errors.login && <span className="form-error-message">{ auth.errors.login }</span> }
          </FormItem>
        </Form>
      </Col>
    </Row>
  )
}

export default connect(
  ({ auth }) => ({ auth }),
  dispatch => ({
    callLogin(username, password) {
      dispatch(login(username, password));
    }
  })
)(Form.create()(Login));