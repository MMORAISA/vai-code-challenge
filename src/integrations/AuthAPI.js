import userMock, { publicUserMock } from '../mock/userMock';

class AuthAPI { 

    static login (username,password) {
        return new Promise((resolve, reject) => {            
            setTimeout(() => {
                if(userMock.access.login === username
                    && userMock.access.password === password) {
                    resolve(publicUserMock);
                } else {
                    reject('Login ou senha inválidos');
                }
            },1000);
        });
    }

    static revalidateToken (token) {
        return new Promise((resolve, reject) => {            
            setTimeout(() => {
                if(userMock.token === token) {
                    resolve(publicUserMock);
                } else {
                    reject();
                }
            },1000);
        });
    }

    static logout () {
        // call some API to log logout by user
        return new Promise(resolve => 
            setTimeout(resolve,1000)    
        );
    }

}

export default AuthAPI;