import { RANDOM_USER_API_ENDPOINT } from './Defaults';
import { DEFAULT_NATIONALITY } from '../utility/constants';

class UsersAPI {

  static async fetchUsers(language, page) {
    const response = await fetch(`${RANDOM_USER_API_ENDPOINT}/?page=${page}&results=10&nat=${language}&seed=vaicodechallenge`);
    const responseJson = await response.json();
    return responseJson.results;
  }

  static async fetchOne(uuid, language = DEFAULT_NATIONALITY) {
    const response = await fetch(`${RANDOM_USER_API_ENDPOINT}/?uuid=${uuid}&nat=${language}&seed=vaicodechallenge`);
    const responseJson = await response.json();
    return responseJson.results[0];
  }

}

export default UsersAPI;