export const publicUserMock = {
    name: 'Murillo de Morais',
    token: 'VAI-CODE-CHALLENGE'
};

export default {
    ...publicUserMock,
    access: {
        login: 'admin',
        password: 'password'
    }
};