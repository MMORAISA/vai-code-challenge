import { all, call, fork, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { CALL_FETCH_USERS, fetchUsersSuccess, fetchUsersFailed, CALL_FETCH_ONE, fetchOneSuccess, fetchOneFailed } from '../ducks/Users';
import UsersAPI from '../integrations/UsersAPI';
import User from '../classes/User';
import { message } from 'antd';

function convertObjectToUser (user) {
    return new User(
        user.gender,
        user.name,
        user.location,
        user.email,
        user.login,
        user.dob,
        user.registered,
        user.phone,
        user.cell,
        user.id,
        user.picture,
        user.nat
    );
}

function* fetchUsers(action) {
    const { language, page } = action;
    try {
        let users = yield call(UsersAPI.fetchUsers, language, page);

        users = users.map(convertObjectToUser);

        yield put(fetchUsersSuccess(users));
    }
    catch(error) {
        yield put(fetchUsersFailed(error));
    }
}

function* fetchUsersSaga() {
    yield takeLatest(CALL_FETCH_USERS, fetchUsers);
}

function* fetchOne(action) {
    try {
        const { uuid } = action;
        const user = yield call(UsersAPI.fetchOne,uuid);

        /* Workaround to resolve API issue: it'snt respecting sent UUID */
        user.login.uuid = uuid;

        yield put(fetchOneSuccess(convertObjectToUser(user)));
    }
    catch (error) {
        yield put(fetchOneFailed(error));
        message.error('Não foi possível obter os dados do usuário');
    }
}

function* fetchOneUserSaga() {
    yield takeEvery(CALL_FETCH_ONE, fetchOne);
}

export default function*() {
    yield all([
        fork(fetchUsersSaga),
        fork(fetchOneUserSaga)
    ]);
}