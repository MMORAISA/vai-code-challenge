import { all, call, fork, put, takeLatest } from 'redux-saga/effects';
import { CALL_LOGIN, loginFailed, loginSuccess, CALL_LOGOUT, expiredToken, REVALIDATE_TOKEN } from '../ducks/Auth';
import AuthAPI from '../integrations/AuthAPI';

function* login(action) {
    try {
        const { username, password } = action;
        const user = yield call(AuthAPI.login, username, password);
        yield put(loginSuccess(user));
    }
    catch(error) {
        yield put(loginFailed(error));
    }
}

function* loginSagas() {
    yield takeLatest(CALL_LOGIN, login);
}

function* logout() {
    yield call(AuthAPI.logout);
}

function* logoutSaga() {
    yield takeLatest(CALL_LOGOUT, logout);
}

function* revalidateToken(action) {
    try {
        const { token } = action;
        const user = yield call(AuthAPI.revalidateToken, token);
        yield put(loginSuccess(user));
    }
    catch(error) {
        yield put(expiredToken());
    }
}

function* revalidateTokenSagas() {
    yield takeLatest(REVALIDATE_TOKEN, revalidateToken);
}

export default function* () {
    yield all([
        fork(loginSagas),
        fork(logoutSaga),
        fork(revalidateTokenSagas)
    ]);
}