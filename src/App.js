import React from 'react';
import 'antd/dist/antd.css';
import './App.css';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import PrivateRoute from './pages/helpers/PrivateRoute';
import Loading from './pages/Loading';
import EditUser from './pages/EditUser';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login" exact component={Login} />
        <PrivateRoute path="/home" exact component={Home} />
        <PrivateRoute path="/edit/:uuid" exact component={EditUser} />
        <Route component={Loading} />
      </Switch>
    </Router>
  );
}

export default App;
