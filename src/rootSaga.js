import { all, fork } from 'redux-saga/effects';
import AuthSagas from './sagas/AuthSagas';
import UserSagas from './sagas/UserSagas';

export default function* () {
    yield all([
        fork(AuthSagas),
        fork(UserSagas)
    ]);
}