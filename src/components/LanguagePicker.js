import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import { DEFAULT_NATIONALITY } from '../utility/constants';

const Option = Select.Option;

function LanguagePicker ({ enabledNationalities, onChangeNationality }) {

    return (
        <Select
            defaultValue={DEFAULT_NATIONALITY}
            onChange={onChangeNationality}
            optionFilterProp="children"
            placeholder="Nationality"
            showSearch
            style={{ width: 110 }} >
            {
                enabledNationalities.map(nationality =>
                    <Option key={nationality} value={nationality}>{nationality}</Option>
                )
            }
        </Select>
    )

}

LanguagePicker.propTypes = {
    enabledNationalities: PropTypes.arrayOf(PropTypes.string).isRequired,
    onChangeNationality: PropTypes.func.isRequired
}

export default LanguagePicker;