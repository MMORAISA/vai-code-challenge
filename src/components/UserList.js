import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchUsers } from '../ducks/Users';
import UserClass from '../classes/User';
import User from './User';
import Paginator from './Paginator';
import './UserList.css';

function UserList({ callFetchUsers, language = 'br', loading, onUserClick, users }) {

  const [page, setPage] = useState(1);

  useEffect(() => {
    callFetchUsers(language, page);
  }, [language, page])

  return (
    <div className="user-list-wrapper">
      <div className="user-list">
        { users.map(user => <User key={user.login.uuid} onClick={onUserClick} user={user} />) }
      </div>
      <div className="paginator-wrapper">
        <Paginator
          disabled={loading}
          onChangePage={page => setPage(page)} />
      </div>
    </div>
  )
}

UserList.propTypes = {
  callFetchUsers: PropTypes.func.isRequired,
  language: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  onUserClick: PropTypes.func.isRequired,
  users: PropTypes.arrayOf(PropTypes.instanceOf(UserClass)).isRequired
};

export default connect(
  null,
  dispatch => ({
    callFetchUsers(language, page) {
      dispatch(fetchUsers(language, page))
    }
  })
)(UserList);