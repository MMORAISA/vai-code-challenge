import React from 'react';
import PropTypes from 'prop-types';
import UserClass from '../classes/User';
import { Card, Avatar } from 'antd';
import { capitalize } from '../utility/utils';
import './User.css';

const { Meta } = Card;

function User ({ onClick, user }) {
    
    return (
        <Card className="user" onClick={() => onClick(user)}>
            <Meta
            avatar={<Avatar src={user.picture.thumbnail} />}
            title={`${capitalize(user.name.title)} ${capitalize(user.name.first)} ${capitalize(user.name.last)}`}
            description={`${user.email} | ${user.cell}`}
            />
        </Card>
    )

}

User.propTypes = {
    user: PropTypes.instanceOf(UserClass).isRequired,
    onClick: PropTypes.func.isRequired
}

export default User;