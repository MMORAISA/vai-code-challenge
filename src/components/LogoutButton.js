import React from 'react';
import { Button, Popconfirm } from 'antd';
import { logout } from '../ducks/Auth';
import { connect } from 'react-redux';
import { clearAuthUser } from '../utility/authState';
import './LogoutButton.css';

function LogoutButton ({ callLogout, history }) {

    const handleLogout = () => {
        callLogout();
        clearAuthUser();
        history.push('/');
    }

    return (
        <Popconfirm
            placement="bottomRight"
            title={"Você tem certeza de que deseja sair?"}
            onConfirm={handleLogout}
            okText="Sim"
            cancelText="Não">
            <Button className="btn-logout" icon="logout">Desconectar</Button>
        </Popconfirm>
    )

}

export default connect(
    null,
    dispatch => ({
        callLogout() {
            dispatch(logout());
        }
    })
)(LogoutButton);