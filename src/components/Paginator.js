import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './Paginator.css';

const classNames = require('classnames');

function Paginator ({ disabled = false, onChangePage }) {

    const [activePage,setActivePage] = useState(1);

    const before = [...Array(5).keys()].map(x => {
        const y = activePage - x - 1;
        return y >= 0 ? y : null;
    }).filter(Boolean);

    const after = [...Array(5).keys()].map(x => 
        activePage + x + 1
    );

    const pages = before.concat([activePage],after).sort((a,b) => a - b)

    const handleChangePage = page => {
        setActivePage(page);
        onChangePage(page);
    }

    return (
        <div className="paginator">
            {
                pages.map(page =>
                    <button
                        className={classNames('paginator-page',{
                            '--is-active': page === activePage
                        })}
                        disabled={disabled}
                        key={page}
                        onClick={() => handleChangePage(page)}>
                        {page}
                    </button>
                )
            }
        </div>
    )

}

Paginator.propTypes = {
    disabled: PropTypes.bool,
    onChangePage: PropTypes.func.isRequired
}

export default Paginator;