const LOCAL_STORE_AUTH_USER_KEY = 'VAI_CODE_CHALLENGE::AUTH_USER';

export const getAuthUser = () => {
    const user = localStorage.getItem(LOCAL_STORE_AUTH_USER_KEY);
    return user ? JSON.parse(user) : null;
};

export const setAuthUser = user => localStorage.setItem(LOCAL_STORE_AUTH_USER_KEY, JSON.stringify(user));

export const clearAuthUser = () => localStorage.removeItem(LOCAL_STORE_AUTH_USER_KEY);