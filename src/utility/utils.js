export const capitalize = text => {
    return text[0].toUpperCase() + text.substr(1);   
}