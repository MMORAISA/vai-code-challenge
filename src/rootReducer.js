import { combineReducers } from 'redux';
import auth from './ducks/Auth';
import users from './ducks/Users';

export default combineReducers({
    auth,
    users
});