import Coordinates from './Coordinates';
import Dob from './Dob';
import Id from './Id';
import Location from './Location';
import Login from './Login';
import Name from './Name';
import Picture from './Picture';
import Registered from './Registered';
import Timezone from './Timezone';

class User {

  constructor(gender, name, location, email, login, dob, registered, phone, cell, id, picture, nat) {
    this.gender = gender;
    this.name = new Name(
      name.title,
      name.first,
      name.last
    );
    this.location = new Location(
      location.street,
      location.city,
      location.state,
      location.postcode,
      new Coordinates(
        location.coordinates.latitude,
        location.coordinates.longitude
      ),
      new Timezone(
        location.timezone.offset,
        location.timezone.description
      )
    );
    this.email = email;
    this.login = new Login(
      login.uuid,
      login.username,
      login.password,
      login.salt,
      login.md5,
      login.sha1,
      login.sha256
    );
    this.dob = new Dob(
      dob.date,
      dob.age
    );
    this.registered = new Registered(
      registered.date,
      registered.age
    );
    this.phone = phone;
    this.cell = cell;
    this.id = new Id(
      id.name,
      id.value
    );
    this.picture = new Picture(
      picture.large,
      picture.medium,
      picture.thumbnail
    );
    this.nat = nat;
  }

}

export default User;