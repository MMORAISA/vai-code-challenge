class Picture {

  constructor(large, medium, thumbnail) {
    this.large = large;
    this.medium = medium;
    this.thumbnail = thumbnail;
  }

}

export default Picture;