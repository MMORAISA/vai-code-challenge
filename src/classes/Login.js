class Login {

  constructor(uuid, username, password, salt, md5, sha1, sha256) {
    this.uuid = uuid;
    this.username = username;
    this.password = password;
    this.salt = salt;
    this.md5 = md5;
    this.sha1 = sha1;
    this.sha256 = sha256;
  }

}

export default Login;