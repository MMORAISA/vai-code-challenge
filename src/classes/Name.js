class Name {

  constructor(title, first, last) {
    this.title = title;
    this.first = first;
    this.last = last;
  }

}

export default Name;