import Coordinates from './Coordinates';
import Timezone from './Timezone';

class Location {

  constructor(street, city, state, postcode, coordinates, timezone) {
    this.street = street;
    this.city = city;
    this.state = state;
    this.postcode = postcode;
    this.coordinates = new Coordinates(
      coordinates.latitude,
      coordinates.longitude
    );
    this.timezone = new Timezone(
      timezone.offset,
      timezone.description
    );
  }

}

export default Location;