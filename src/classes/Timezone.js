class Timezone {

  constructor(offset, description) {
    this.offset = offset;
    this.description = description;
  }

}

export default Timezone;