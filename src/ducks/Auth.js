import { DEFAULT_NATIONALITY } from '../utility/constants';

/* Action Types */
export const CALL_LOGIN = 'CALL_LOGIN';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const CALL_LOGOUT = 'CALL_LOGOUT';
export const REVALIDATE_TOKEN = 'REVALIDATE_TOKEN';
export const EXPIRED_TOKEN = 'EXPIRED_TOKEN';
export const LANGUAGE_CHANGE = 'LANGUAGE_CHANGE';

/* Initial State */
const INITIAL_STATE = {
    triedTokenRevalidation: false,
    errors: {
        login: null
    },
    loading: {
        login: false
    },
    user: null
};

/* Reducer */
export default function reducer(state = INITIAL_STATE, action) {
    const { error, language, type, user } = action;

    switch(type) {
        case CALL_LOGIN:
            return {
                ...state,
                errors: {
                    ...state.errors,
                    login: null
                },
                loading: {
                    ...state.loading,
                    login: true
                }
            };
        case LOGIN_FAILED:
            return {
                ...state,
                errors: {
                    ...state.errors,
                    login: error
                },
                loading: {
                    ...state.loading,
                    login: false
                }
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                errors: {
                    ...state.errors,
                    login: null
                },
                loading: {
                    ...state.loading,
                    login: false
                },
                triedTokenRevalidation: true,
                user: {
                    ...user,
                    language: DEFAULT_NATIONALITY
                }
            };
        case CALL_LOGOUT:
            return {
                ...state,
                user: null
            };
        case REVALIDATE_TOKEN: 
            return {
                ...state,
                loading: {
                    ...state.loading,
                    login: true
                }
            };
        case EXPIRED_TOKEN:
            return {
                ...state,
                triedTokenRevalidation: true,
                loading: {
                    ...state.loading,
                    login: false
                }
            };
        case LANGUAGE_CHANGE:
            return {
                ...state,
                user: {
                    ...user,
                    language
                }
            }
        default:
            return state;
    }
}

/* Action Creators */
export const login = (username, password) => {
    return { type: CALL_LOGIN, username, password };
}

export const loginFailed = error => {
    return { type: LOGIN_FAILED, error };
}

export const loginSuccess = user => {
    return { type: LOGIN_SUCCESS, user };
}

export const logout = () => {
    return { type: CALL_LOGOUT };
}

export const revalidateToken = token => {
    return { type: REVALIDATE_TOKEN, token };
}

export const expiredToken = () => {
    return { type: EXPIRED_TOKEN };
}

export const languageChange = language => {
    return { type: LANGUAGE_CHANGE, language };
}