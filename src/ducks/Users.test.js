import Users, {
  CALL_FETCH_USERS, fetchUsers,
  FETCH_USERS_FAILED, fetchUsersFailed,
  FETCH_USERS_SUCCESS, fetchUsersSuccess
} from './Users';

describe('(Ducks) Users', () => {

  describe('Action Creators', () => {

    it('fetchUsers returns a CALL_FETCH_USERS action', () => {
      expect(fetchUsers().type)
        .toBe(CALL_FETCH_USERS);
    });

    it('fetchUsersFailed returns a FETCH_USERS_FAILED action', () => {
      expect(fetchUsersFailed().type)
        .toBe(FETCH_USERS_FAILED);
    });

    it('fetchUsersSuccess returns a FETCH_USERS_SUCCESS action', () => {
      expect(fetchUsersSuccess().type)
        .toBe(FETCH_USERS_SUCCESS);
    });

  });

  describe('Reducer', () => {

    it('returns state even if action is unknown', () => {
      const state = {};
      expect(Users(state, {
        type: 'UNKNOWN_ACTION'
      })).toBe(state);
    });

    describe('Data', () => {

      it('fetchUsersSuccess sets data', () => {
        const users = [
          { id: 1, name: 'Test 1' }
        ];
        const state = {
          data: []
        };
        expect(
          Users(state, fetchUsersSuccess(users)).data[0]
        ).toMatchObject(users[0]);
      });

    });

    describe('Errors', () => {

      it('fetchUsersFailed sets errors.fetch', () => {
        const error = new Error();
        const state = {
          errors: {
            fetch: null
          }
        };
        expect(
          Users(state, fetchUsersFailed(error)).errors.fetch
        ).toMatchObject(error);
      });

    });

    describe('Loadings', () => {

      it('fetchUsers sets loading.fetch to true', () => {
        const state = {
          loading: {
            fetch: false
          }
        };
        expect(
          Users(state, fetchUsers()).loading.fetch
        ).toBe(true);
      });

      it('fetchUsersFailed sets loading.fetch to false', () => {
        const state = {
          loading: {
            fetch: true
          }
        };
        expect(
          Users(state, fetchUsersFailed()).loading.fetch
        ).toBe(false);
      });

      it('fetchUsersSuccess sets loading.fetch to false', () => {
        const state = {
          loading: {
            fetch: true
          }
        };
        expect(
          Users(state, fetchUsersSuccess()).loading.fetch
        ).toBe(false);
      });

    });

  });

});