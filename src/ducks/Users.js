/* Action Types */
export const CALL_FETCH_USERS = 'CALL_FETCH_USERS';
export const FETCH_USERS_FAILED = 'FETCH_USERS_FAILED';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const CALL_FETCH_ONE = 'CALL_FETCH_ONE';
export const FETCH_ONE_FAILED = 'FETCH_ONE_FAILED';
export const FETCH_ONE_SUCCESS = 'FETCH_ONE_SUCCESS';
export const DELETE_USER = 'DELETE_USER';

/* Initial State */
const INITIAL_STATE = {
  data: [],
  errors: {
    fetch: null,
    fetchOne: null
  },
  loading: {
    fetch: false,
    fetchOne: false
  }
}

/* Reducer */
export default function reducer(state = INITIAL_STATE, action) {
  const { error, user, users, uuid, type } = action;

  switch (type) {
    case CALL_FETCH_USERS:
      return {
        ...state,
        errors: {
          ...state.errors,
          fetch: null
        },
        loading: {
          ...state.loading,
          fetch: true
        }
      };
    case FETCH_USERS_FAILED:
      return {
        ...state,
        errors: {
          ...state.errors,
          fetch: error
        },
        loading: {
          ...state.loading,
          fetch: false
        }
      };
    case FETCH_USERS_SUCCESS:
      return {
        ...state,
        data: users,
        errors: {
          ...state.errors,
          fetch: null
        },
        loading: {
          ...state.loading,
          fetch: false
        }
      };
    case CALL_FETCH_ONE:
      return {
        ...state,
        errors: {
          ...state.errors,
          fetchOne: null
        },
        loading: {
          ...state.loading,
          fetchOne: true
        }
      };
    case FETCH_ONE_FAILED:
      return {
        ...state,
        errors: {
          ...state.errors,
          fetchOne: error
        },
        loading: {
          ...state.loading,
          fetchOne: false
        }
      };
    case FETCH_ONE_SUCCESS:
      return {
        ...state,
        errors: {
          ...state.errors,
          fetchOne: null
        },
        loading: {
          ...state.loading,
          fetchOne: false
        },
        data: state.data.concat([user])
      };
    case DELETE_USER:
      return {
        ...state,
        data: state.data.filter(user => user.login.uuid !== uuid)
      };
    default:
      return state;
  }
}

/* Action Creators */
export const fetchUsers = (language, page) => ({
  type: CALL_FETCH_USERS,
  language,
  page
});

export const fetchUsersFailed = error => ({
  type: FETCH_USERS_FAILED,
  error
});

export const fetchUsersSuccess = users => ({
  type: FETCH_USERS_SUCCESS,
  users
});

export const fetchOne = uuid => ({
  type: CALL_FETCH_ONE,
  uuid
});

export const fetchOneFailed = error => ({
  type: FETCH_ONE_FAILED,
  error
});

export const fetchOneSuccess = user => ({
  type: FETCH_ONE_SUCCESS,
  user
});

export const deleteUser = uuid => ({
  type: DELETE_USER,
  uuid
});